<?php

namespace Drupal\twig_webp;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Twig extension.
 */
class TwigWebpTwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    $filters[] = new TwigFilter(
      'towebp',
      [ToWebp::class, 'toWebpFilter'],
    );
    return $filters;
  }

}
