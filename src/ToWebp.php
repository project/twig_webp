<?php

namespace Drupal\twig_webp;

use Drupal\Component\Utility\UrlHelper;

/**
 * Class for converting an image to webp.
 */
class ToWebp {

  /**
   * Function to convert an imnage into an webp.
   *
   * @param object $url
   *   Image URL.
   *
   * @return string
   *   Webp Image URL.
   */
  public static function toWebpFilter($url): string {
    // If url is empty or external do not convert.
    if (empty($url) || UrlHelper::isExternal($url)) {
      return $url;
    }

    $parsed_url = UrlHelper::parse($url);
    $source_url_path = DRUPAL_ROOT . $parsed_url['path'];

    if (!is_file($source_url_path)) {
      return $url;
    }

    $ext = self::isSvg($source_url_path);

    // Return if image is svg.
    if ($ext === TRUE) {
      return $url;
    }

    $destination_path = str_replace($ext, "webp", $source_url_path);
    $final_url = str_replace($ext, "webp", $parsed_url['path']);

    if (is_file($destination_path)) {
      return $final_url;
    }

    if (function_exists('imagewebp')) {
      $imagecreate_function = 'imagecreatefrom' . $ext;
      if (!function_exists($imagecreate_function)) {
        return $url;
      }

      $input = $imagecreate_function($source_url_path);
      if (imagewebp($input, $destination_path)) {
        return $final_url;
      }
    }

    return $url;
  }

  /**
   * Check whether current file is svg or not.
   *
   * @param string $file_path
   *   Path of the file.
   *
   * @return bool|string
   *   True if its SVG or extension name.
   */
  private static function isSvg(string $file_path): bool|string {
    $ext = pathinfo($file_path, PATHINFO_EXTENSION);
    return ($ext === 'svg') ? TRUE : $ext;
  }

}
