## INTRODUCTION

The twig_webp module provides a twig filter to convert any image into webp format in twig.

## REQUIREMENTS

PHP GD or PHP Image magic library.

## MAINTAINERS

Current maintainers for Drupal 10:

- Gaurav Goyal (gaurav.goyal) - https://www.drupal.org/u/gauravgoyal-0
